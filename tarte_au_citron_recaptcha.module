<?php

/**
 * @file
 * Provides primary Drupal hook implementations.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_element_info_alter().
 */
function tarte_au_citron_recaptcha_element_info_alter(array &$info) {
  if (isset($info['captcha'])) {
    $info['captcha']['#process'][] = '_tarte_au_citron_recaptcha_process';
  }
}

/**
 * Process captcha element to check if it's a recaptcha and remove js.
 *
 * @param array $element
 *   The form element captcha.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state.
 * @param array $complete_form
 *   The form.
 *
 * @return null|array
 *   The element.
 */
function _tarte_au_citron_recaptcha_process(array &$element, FormStateInterface $form_state, array &$complete_form) {
  /**
   * @var \Drupal\tarte_au_citron\ServicesManagerInterface $serviceManager
   */
  $serviceManager = \Drupal::getContainer()
    ->get('tarte_au_citron.services_manager');
  if (!$serviceManager->isNeeded()) {
    return;
  }

  if ($serviceManager->isServiceEnabled('drupal_recaptcha') && isset($element['captcha_widgets']['recaptcha_widget'])) {
    unset($element['captcha_widgets']['recaptcha_widget']['#attached']['html_head']);
  }
  return $element;
}
